# Deep Neural Networks

- A deep neural network (DNN) is an artificial neural network (ANN) with multiple layers between the input and output layers.
- DNN is one of the deep learning architectures.
- Other deep learning architectures are deep belief network, convolutional neural network and recurrent neural network.

![Deep Neural Network](images/deepnet.png)

## Neural Networks
If we want to recognize the images of handwritten digits using neural network, then those images should be feed to the neural network.

![handwritten digits](images/handwrit.png)


- Consider all of these handwritten digit images have size 28x28 pixels.
- These 28x28 = 784 pixels of the image will act as neurons forming the first layer of the network.
- Each neuron holds a number that represents the grayscale value of the corresponding pixel ranging from 0 for black pixels upto 1 for white pixels.
- Last layer of the network consists of ten neurons each representing one of the digits (0,1,2,3,4,5,6,7,8,9).
- Between input and output, there are couple of layers called hidden layers.

Here, in this neural network, every other neuron in the 2nd layer will be connected to all 784 pixel neurons from the 1st layer and all 784 connections has its own weight and each neuron in second layer has some bias.

If we have the network architecture such as:    
**Input layer = 784 neurons**  
**Hidden layers = 2 layers each having 16 neurons**  
**Output layer = 10 neurons**  

Then, we have 784x16 + 16x16 + 16x10 weights  
and 16 + 16 + 16 biases. Total 13,002 weights and biases.  

### Weights
- Weight is assigned to each one of the connections between the neuron in 2nd layer and the neurons from the 1st layer.
- Detect the pixel pattern in the images such as egdes, loops, lines and whole digit.
- Weighted sum is calculated as :    
w<sub>1</sub>a<sub>1</sub> + w<sub>2</sub>a<sub>2</sub> + w<sub>3</sub>a<sub>3</sub> + . . . + w<sub>n</sub>a<sub>n</sub>  

![weighted sum](images/weights.png)

### Bias
- To activate the neuron only when the weighted sum value is greater than some specific value, bias is added to the weighted sum such as :    
w<sub>1</sub>a<sub>1</sub> + w<sub>2</sub>a<sub>2</sub> + w<sub>3</sub>a<sub>3</sub> + . . . + w<sub>n</sub>a<sub>n</sub> + b  
where b = bias    
- Bias is the negative value such as -10.

### Activation fucntion
- Weighted sum can come out to be any number. 
- To want that value to be in range of 0 and 1, the weighted sum needs to be passed through the sigmoid squishification function.

![Sigmoid squishification function](images/sigmoid.jpeg)

- For -ve inputs, sigmoid function ends up close to 0 and for +ve ones, ends up close to 1.

### Matrix representation of weights, biases and neurons
- All the activations or neurons from one layer are organized into column as vector.
- All the weights are organized into a matrix where each row of matrix corresponds to the connections between one layer and particular neuron in the next layer.
- All the biases are organized into a vector.
- Weight matrix is multiplied with activations vector and finally bias vector is added to this matrix vector product.

![Matrix Representation](images/matrix.png)

## Cost function
- Suppose, we feed the image of digit 3 to the neural network but the output layer does not recognize it as 3, then in such situation to tell the network that what it is doing is wrong and to give it a method to correct its output, we define a cost function.
- Cost function = summation of the squares of the differences between actual output activations and the required output value.
- When network correctly classifies the image then value from cost function is small, else the value is large.

![cost function](images/cost.png)

## Gradient Decent
- Gradient descent of a function is the negative gradient of function that gives the directiion to step that decreases the function more quickly.
- It tells how we need to change all these weights and biases so as to most efficiently decrease the cost function.
- Magnitude of each component of gradient descent tells how sensitive the cost function is to each weight and bias such as:
Cost function is 32 times more sensitive to the change in weight 3.20 than to the change in weight 0.10.

![Gradient Descent](images/gradient.png)

## Backpropagation
Suppose we want the neural network to classify an image as 2, then we want the third value to get nudged up and all the other values to get nudged down such as :

![digit 2 recognition](images/digit2.png)

- Three different avenues that increase the activation of neuron 2 are :   
   **Increase bias b**  
   **Increase weight w<sub>i</sub>**  
   **Change activations from the previous layer a<sub>i</sub>**  
 
- To get the output as digit 2, everything connected to digit 2 neuron with +ve weight shoud get brighter and with a -ve weight should get dimmmer. And also other neurons from the last layer should be less active.
- Neurons activations cannot be directly influenced, only weights and biases can be tweaked or changed which inturn changes the activations.
- Each of the output neuron has its own desire about what should happen to second-to-last layer in proportion to the corresponding weights. This is where the idea of back propagating comes in.
- Backpropagation is an algorithm for determining how a single training example would like to nudge the weights and biases.
- By adding all these desires of output neurons, we get a list of nudges that we want to happen in second-to-last layer.
- After having those values, same process can be applied recursively to the relevant weights and biases that determine those values.
- Training data is subdivided into mini-batches say each mini-batch containing 100 examples, and gradient descent is computed for each mini-batch by applying backpropagation algorithm.
- Applying gradient descent to all of the mini-batches  will give pretty much good approximation and making these adjustments, we will converge towards a local minimum of the cost function. 
- And finally networks ends up doing really good in training examples.


