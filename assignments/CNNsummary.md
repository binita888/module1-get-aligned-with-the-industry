# Convolutional Neural Network
- A convolutional neural network is a specialized type of neural network model designed for working with two-dimensional image data, although they can be used with one-dimensional and three-dimensional data.
- CNNs are basically just several layers of convolutions with nonlinear activation functions like ReLU or tanh applied to the results.

![Convolutional Neural Network](images/convo.jpeg)
       
## Building Blocks of CNN
1. Convolutional Layer
2. ReLu Layer
3. Pooling Layer
4. Fully Connected Layer

### Convolutional Layer
Convolutional layer is the major building block of CNN which performs the operation called convolution. It consists of filters and feature maps.
This is the first layer to extract features from the input image.

#### Convolution :
The convolution operation is a dot product of the original pixel values in input image with weights defined in the filter. The result is summed up and divided by the number of pixels in the filter.

 ![Convolution]( ) <img src="images/convolution.png"  width="600" height="300">
 
#### Filter :
The filters are the “neurons” of the layer having weights. Filters have size smaller than the input image. Filters detect whether the certain features are  present in the image or not. 
Various types of filters are : Line Filter, Edge Filter, Blur Filter, Sharpen Filter, etc.

![Edge detection and sharpen filters ]( ) <img src="images/filter1.png"  width="500" height="250">

![Blur and vertical line detector filter]( ) <img src="images/filter2.png"  width="500" height="250">

#### Feature Map :
The output from multiplying the filter with the input array one time is a single value. Multiplying the filter multiple times with the input array results a two-dimensional array of output values which is called as a feature-map. 

Size of output image after convolution :

![Feature map size](images/feature.png)
 
 where W and H are width and height of input image,
 F~w~ and F~h~ are filter width and height,
 P = Padding size,
 S~w~ and S~h~ are stride value in horizontal and vetical direction.
 
     

#### Stride :
Stride is the number of pixels a filter shifts over the input matrix. When the stride is 1 then filter is moved 1 pixel at a time and when it is 2, it is moved 2 pixels at a time and so on.

#### Padding :
Padding is simply a process of adding the layers of zeros to the input image so that the filter can perfectly convolve over the entire input image.

![zero padding](images/padding.png)

- **Valid Padding** : No padding at all. Drop the part of the image where the filter did not fit. 

- **Same Padding** : Adding the padding layers such that the output image has the same dimension as the input image.


### ReLU Layer
After the convolution operation, the generated feature maps are passed through the ReLu layer.  ReLu is an activation function which only activates when the input is above certain quantity. 

![Relu Function](images/relu.png)

### Pooling Layer
In this layer, image is shrinked into smaller size. Pooling is done after passing through the activation layer.

- Pick a window size (usually 2 or 3)
- Pick a stride (usually 2)
- Slide window across filtered images
- From each window, take the maximum value 

**Max Pooling**
![Max Pooling](images/maxPool.png)

**Average Pooling**
![Average Pooling](images/avgPool.png)

### Fully Connected Layer
- Fully connected layers are the normal flat feed-forward neural network layer.
- This layer connect every neuron in one layer to every neuron in another layer.
- This layer have a non-linear activation function or a softmax activation in order to output probabilities of class predictions.
- Used at the end of the network after feature extraction.
- Used to create final non-linear combinations of features and for making predictions by the network.

![Fully Connected Layer](images/fully.png)


#### Flattening
- Flattening is converting the data into a 1-dimensional array for inputting it to the next layer.
- Output of the convolutional layers are flattened to create a single long feature vector.
- And it is connected to the a fully-connected layer.

![Flattened]( ) <img src="images/flatten.png"  width="400" height="300">

## Applications of CNN
- **Agriculture :** 
Pictures of crops are taken using hyperspectral or multispectral sensors and analyzed with computer vision to determine their health, or the viability of seeds to be sown.

- **Self-driving cars :** 
CNNs are used for object detection and classification, performed in real time against live video footage from car cameras.

- **Healthcare :**
Diseases like pneumonia, diabetes and breast cancer can be diagnosed using CNN. In many cases CNN-based analysis and diagnosis of medical images can be as accurate or even more accurate than a human technician or physician.
- **Surveillance :** 
Modern security systems with computer vision capabilities can identify crime, violence or theft in video footage in real time and alert security personnel.
